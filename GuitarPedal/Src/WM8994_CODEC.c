#include "WM8894_CODEC.h"
#include "i2c.h"

HAL_StatusTypeDef error;

/**
  \fn          int32_t WM8994_RegWrite (uint8_16 reg, uint16_t reg_val)
  \brief       Write value to WM8994 register
  \param[in]   reg      WM8994 register address
  \param[in]   reg_val  Value to be written to WM8994 register
  \returns
   - \b  0: function succeeded
   - \b -1: function failed
*/
void WM8994_RegWrite(uint16_t reg, uint16_t reg_val) 
{
  uint8_t   buf[4] = {0};

  buf[0] = (uint8_t)(reg     >> 8);
  buf[1] = (uint8_t)(reg         );
  buf[2] = (uint8_t)(reg_val >> 8);
  buf[3] = (uint8_t)(reg_val     );
	
	error = HAL_I2C_Master_Transmit(&hi2c3,WM8994_I2C_ADDR,buf,4,1000);
	
}

/**
  \fn          int32_t WM8994_RegRead (uint16_t reg, uint16_t * reg_val)
  \brief       Read value from WM8994 register
  \param[in]   reg      WM8994 register address
  \param[Out]  reg_val  Pointer to value read from WM8994 register
  \returns
   - \b  0: function succeeded
   - \b -1: function failed
*/
void WM8994_RegRead(uint16_t reg, uint16_t *reg_val) 
{
  uint8_t  buf[2] = {0}, reg_addr[2];

  reg_addr[0] = (uint8_t)(reg >> 8);
  reg_addr[1] = (uint8_t)reg;
	
	error = HAL_I2C_Master_Transmit(&hi2c3,WM8994_I2C_ADDR,reg_addr,2,1000);
	if (error == HAL_OK)
	{
		error = HAL_I2C_Master_Receive(&hi2c3, WM8994_I2C_ADDR,buf,2,1000);
	}
	
  *reg_val  = buf[0] << 8;
  *reg_val |= buf[1];

}

/**
  \fn          int32_t WM8994_RegModify (uint8_16 reg, uint16_t mask, uint16_t val)
  \brief       Write value to WM8994 register
  \param[in]   reg      WM8994 register address
  \param[in]   mask     Mask bits to be changed
  \param[in]   val      Value of masked bits
  \returns
   - \b  0: function succeeded
   - \b -1: function failed
*/
void WM8994_RegModify (uint16_t reg, uint16_t mask, uint16_t val) 
{
  uint8_t   buf[4];
  uint16_t  rd_val;

  WM8994_RegRead (reg, &rd_val);
  val |= (rd_val & ~mask);

  buf[0] = (uint8_t)(reg >> 8);
  buf[1] = (uint8_t)(reg     );
  buf[2] = (uint8_t)(val >> 8);
  buf[3] = (uint8_t)(val     );

	error = HAL_I2C_Master_Transmit(&hi2c3,WM8994_I2C_ADDR,buf,4,1000);

}
