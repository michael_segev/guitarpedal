/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "i2c.h"
#include "sai.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "WM8894_CODEC.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

uint32_t audioData[10];

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	uint16_t buf = 0;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C3_Init();
  MX_SAI2_Init();

  /* USER CODE BEGIN 2 */

	//talk to CODEC

  // WM8994 Reset
  WM8994_RegWrite (0x0000, 0x0000);
	HAL_Delay(500);

  // WM8994 ReadID
  WM8994_RegRead (0x0000, &buf);
	printf("%X\n" , buf);
	

////  // Errata Work-Arounds
//  WM8994_RegWrite(0x0102, 0x0003);
//  WM8994_RegWrite(0x0817, 0x0000);
//  WM8994_RegWrite(0x0102, 0x0000);


  // VMID Soft fast start, VMID buffer enabled
  WM8994_RegWrite(0x0039, 0x006C); //happy


  // Enable Bias Generator, Enable VMID
  WM8994_RegWrite(0x0001, 0x0003); //happy //13 ??

  HAL_Delay(50);

  // Enable AIF1DAC1(left and right) input path (Timeslot 0), Enable ADAC1(left and right)
  WM8994_RegWrite(0x0005, 0x0303); //happy

  // Enable AIF1 (Timeslot 0, left) to DAC1L
  WM8994_RegWrite(0x0601, 0x0001); //happy

  // Enable AIF1 (Timeslot 0, Right) to DAC1R
  WM8994_RegWrite(0x0602, 0x0001); //happy

	
 // Enable ADCL and ADCR, Enable AIF1ADC1 (left) and AIF1ADC1 (right) path
  WM8994_RegWrite (0x0004, 0x0303); 

  // Enable IN1L and IN1R, Enable Thermal sensor & shout down
  WM8994_RegWrite (0x0002, 0x6350);

  // Enable the ADCL(Left) to AIF1 Timeslot 0 (Left) mixer path
  WM8994_RegWrite (0x0606, 0x0002);

  // Enable the ADCR(Right) to AIF1 Timeslot 0 (Right) mixer path
  WM8994_RegWrite (0x0607, 0x0002);

  // GPIO1 Function = AIF1 DRC1
  WM8994_RegWrite (0x0700, 0x000D); //configures the interrupt signal to go high if there is signal presence

  // Enable Bias generator and VMID
  WM8994_RegModify(0x0001, 0x0003, 0x0003); //should still be on //check ...??
	WM8994_RegRead (0x0001, &buf);
	printf("%X\n" , buf);

	
	
  WM8994_RegWrite (0x0018, 0x000B); //left line 1 input volume (0dB - > 1V RMS)
	WM8994_RegRead (0x0018, &buf);
	printf("%X\n" , buf);	
  WM8994_RegWrite (0x001A, 0x000B); //right line 1 input volume (0dB - > 1V RMS)
	WM8994_RegRead (0x001A, &buf);
	printf("%X\n" , buf);	
	
  WM8994_RegWrite (0x0029, 0x0025); //input mixer. Input mixer gain Left = 0dB
	WM8994_RegRead (0x0029, &buf);
	printf("%X\n" , buf);	
  WM8994_RegWrite (0x002A, 0x0025); //input mixer. Input mixer gain Right = 0dB
	WM8994_RegRead (0x002A, &buf);
	printf("%X\n" , buf);	
  // IN1L PGA Inverting Input Select, IN1R PGA Inverting Input Select
  WM8994_RegWrite (0x0028, 0x0011); //make input mixer take audio from IN1RN pin and IN1LN
	WM8994_RegRead (0x0028, &buf);
	printf("%X\n" , buf);
  // Enable AIF1ADC1 Digital HPF
  WM8994_RegWrite(0x0410, 0x1800); //Hi-Fi Mode (removes anything below 4Hz)
	WM8994_RegRead (0x0410, &buf);
	printf("%X\n" , buf);	
  // default sample rate = 16kHz; AIF1CLK/fs ratio = 256
	//WM8994_RegWrite(0x0210, 0x0033);

	//Determines core clock speed ( must be < 12.5 Mhz, > 4.096 MHz and > 256 AIF1 sample rate )
  //sample rate = 48kHz; AIF1CLK/fs ratio = 256 // system clock is 12.288 MHz
  WM8994_RegWrite(0x0210, 0x0083);
	WM8994_RegRead (0x0210, &buf);
	printf("%X\n" , buf);	
  // Right ADC data is output on right channel, Digital audio interface = I2S,
  // Word Length = 16bits, changes from the default 24 bit
  //WM8994_RegWrite(0x0300, 0x4010);
	WM8994_RegRead (0x0300, &buf);
	printf("%X\n" , buf);
  // AIF1 Slave mode (default)
  WM8994_RegWrite(0x0302, 0x0000);
	WM8994_RegRead (0x0302, &buf);
	printf("%X\n" , buf);
  // Enable AIF1 Processing clock and Digital mixing processor clock
  WM8994_RegWrite(0x0208, 0x000A);
	WM8994_RegRead (0x0208, &buf);
	printf("%X\n" , buf);
  // AIF1CLK Source = MCLK1, Enable AIF1CLK
  WM8994_RegWrite(0x0200, 0x0001);
	WM8994_RegRead (0x0200, &buf);
	printf("%X\n" , buf);	

  // Enable SPKMIXL and SPKMIXR output mixer
  WM8994_RegModify(0x0003, 0x0300, 0x0300);
	WM8994_RegRead (0x0003, &buf);
	printf("%X\n" , buf);	
  // Un-mute Left speaker mixer volume control
  WM8994_RegWrite(0x0022, 0x0000);
	WM8994_RegRead (0x0022, &buf);
	printf("%X\n" , buf);
  // Un-mute Right speaker mixer volume control
  WM8994_RegWrite(0x0023, 0x0000);
	WM8994_RegRead (0x0023, &buf);
	printf("%X\n" , buf);
  // UN-mute DAC1 to Speaker mixer
  WM8994_RegWrite(0x0036, 0x0003);
	WM8994_RegRead (0x0036, &buf);
	printf("%X\n" , buf);
  // Enable Mixer, SPKRVOL PGA and SOKOUTR output
  // Enable SPKMIXR Mixer, SPKLVOL PGA  and SPKOUTL output
	WM8994_RegRead (0x0001, &buf);
	printf("%X\n" , buf);	
  WM8994_RegModify(0x0001, 0x3000, 0x3000);
	//WM8994_RegWrite(0x0001, 0x3003);
	WM8994_RegRead (0x0001, &buf);
	printf("%X\n" , buf);	
	
  // Digital audio source for envelope tracking: AIF1, DAC Timeslot 0
  // Enable Class W
  //WM8994_RegWrite(0x0051, 0x0005); //enable dynamic gain
  // Un-mute AIF1 Timeslot 0 path


  // Un-mute DAC1L, DAC1L Digital Volume = 0dB
  WM8994_RegWrite(0x0610, 0x00C0);
	WM8994_RegRead (0x0610, &buf);
	printf("%X\n" , buf);
  // Un-mute DAC1R, DAC1R Digital Volume = 0dB
  WM8994_RegWrite(0x0611, 0x00C0);
	WM8994_RegRead (0x0611, &buf);
	printf("%X\n" , buf);
  // Un-mute AIF1DAC1 input path (AIF1 Timeslot0)
  WM8994_RegWrite(0x0420, 0x0000);
	WM8994_RegRead (0x0420, &buf);
	printf("%X\n" , buf);
	
	//registers are not being set correctly ... check where first mismatch occurs
	
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		HAL_SAI_Receive(&hsai_BlockA2,(uint8_t*)audioData,10,1000);
	
		
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 9;
  RCC_OscInitStruct.PLL.PLLN = 225;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI2|RCC_PERIPHCLK_I2C3;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 192;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV2;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_2;
  PeriphClkInitStruct.Sai2ClockSelection = RCC_SAI2CLKSOURCE_PLLSAI;
  PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
