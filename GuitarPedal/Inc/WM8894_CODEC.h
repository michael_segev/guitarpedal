#ifndef WCODEC
#define WCODEC
#include "stm32f7xx_hal.h"

//#define WM8994_I2C_ADDR        0x1A      // WM8994 I2C address
#define WM8994_I2C_ADDR  0x34

void WM8994_RegWrite(uint16_t reg, uint16_t reg_val);
void WM8994_RegRead(uint16_t reg, uint16_t *reg_val) ;
void WM8994_RegModify (uint16_t reg, uint16_t mask, uint16_t val);

#endif
